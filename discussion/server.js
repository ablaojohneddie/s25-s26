//[SSECTION] CREATING A BASIC SERVER SETUP
	
//Node JS => Will provide us with a "runtime environment (RTE)" that will alllow us to execute our program/application

	//RTE (Runtime Environment) -> we are pertaining to an environment/system in which a program can be executed.

	//TASK: let's create a standard server setup using 'plain' NodeJS.
		//1. Identify and prepare the components/ingredients that you would need in order to execute the task.
			//the main ingredient when creating a server using plain Node is: *http* (hypertext transfer protocol)
			//http - is a 'built in' module of NODE JS, that will allow us to 'establish a connection' and will make it possible to transfer data over HTTP.

			//you need to be able to get the components first, we will use the require() derectie -> this is function that will allow us to gather and acquire certsin packages that we will use to build our application

			const http = require('http');
			//http will provide us with all the components needed to establish a server

			//http contains the utility constructor called 'createServer()' -> which will allow us to create an HTTP server.
			http.createServer()

		//2. Identify and describe a location where the connection will happen, In order to provide a proper medium for both parties (client and server), we will then bind the connection to the desired port number
		let port = 3000;


		//3. Inside the server contructor, insert a method in which we can use in order describes the connect that was established, identify the interaction between the client and the server and pass them down as the arguments of the method

		http.createServer((request, response) => {

		//4. Bind/assign the connection to the address, the listen() is used to bind and listen to a specific port whenever its being access by the computer.

		//write() -> this will allow to insert messages or input to our page.
		response.write(`Maligayang Bati to Port ${port}`);
		response.write('Magandang Hapon Batch 165');
		//end() -> will allow us to identify a point where the transmission of data will end.
		response.end();
		}).listen(port);

	console.log('Server is Running');


	// NOTE: When creating project, we should always think of more efficient



	//TASK: Integrate the use of an NPM Project in NodeJs.
		//1. INITALIZE an NPM(Node Package Manager) into the local project.
			//(2 ways to perform this task)
				//METHOD 1: npm init
				//METHOD 2: npm init -y(yes)
				//package.json -> 'The heart of any Node project', it records all the important meta data about the project, (libraries, packages, function)


			//We have already integrated an NPM into our project, lets solve some issues we are facing in our local project.
				//issues: needing to restart the project whenever changes occur.
				//solve: using NPM lets install a dependency that will allow to authomatically fix (notflix)


				//=> 'nodemon' package

				// BASIC SKILLS using NPM

				//-> install package
					//npm install <name of package>

				//-> uninstall or remove a package